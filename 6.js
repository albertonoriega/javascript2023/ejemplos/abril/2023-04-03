// Dibujar otro div debajo del original

// CON innerHTML
document.querySelector('body').innerHTML += '<div>Final</div>';

// Creando nuevos elementos
const div = document.createElement("div");
div.id = 'cajaFinal';
document.querySelector('body').appendChild(div);
div.innerHTML = 'Final';
// //Lo mismo que el innerHTML
// div.textContent = "Final";
