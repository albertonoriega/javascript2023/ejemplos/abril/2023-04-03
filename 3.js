let pieza = {
    // propiedades
    x: 0,
    y: 0,
    ancho: 100,
    alto: 100,
    color: '#ccc',
    // metodos
    dibujar: function () {
        let salida = "";
        salida = `${this.ancho}, ${this.alto}, ${this.x}, ${this.y}, ${this.color}`;
        return salida;
    },
    // Metodo que se va a ejecutar automáticamente cuando intente tratar el objeto como un texto
    toString: function () {
        let salida = "";
        salida = `${this.ancho}, ${this.alto}, ${this.x}, ${this.y}, ${this.color}`;
        return salida;
    }
};

// Leer el color
console.log(pieza.color);
// Acceder al metodo de dibujar
console.log(pieza.dibujar());
console.log(pieza);

document.write(pieza); // Llama al metodo toString para llamar al contenido del objeto