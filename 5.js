// Crear un objeto llamado Persona con las siguientes propiedades:
//nombre
//direccion
//telefono
// y los siguientes metodos:
// hola => dice hola
//dormir => dice adios
//presentar => nombre y direccion

let persona = {
    nombre: 'Juan',
    direccion: 'C/ Alta 23',
    telefono: 656565632,
    hablar: function () {
        alert("Hola");
    },
    dormir: function () {
        alert("Adios");
    },
    presentar: function () {
        alert(`Mi nombre es: ${this.nombre} y mi direccion es ${this.direccion}`)
    },
};
console.log(persona);
persona.hablar();
persona.dormir();
persona.presentar();
