// OBJETO
let posiciones = {
    x: 0,
    y: 0,
};

//Objeto vacio
let posicionesMalo = {
};
// Modificando el valor de la propiedad X
posiciones['x'] = 10; // utilizando nomenclatura de arrays
posiciones.x = 10 // Utizando la nomenclatura de objetos

// leer la propiedad y
console.log(posiciones['y']); // utilizando nomenclatura de arrays
console.log(posiciones.y); // Utizando la nomenclatura de objetos

// Añadir una propiedad nueva al objeto

posiciones['z'] = 0;

// al asignar un objeto se realiza por referencia
posicionesMalo = posiciones;

// Al asignarse por referencia lo que toques en posicionesMalo se realiza también en posiciones y viceversa
posicionesMalo.z = 20;
