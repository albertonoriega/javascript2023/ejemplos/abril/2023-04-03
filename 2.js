// OBJETO
let posiciones = new Object();

// Modificando el valor de la propiedad X
posiciones['x'] = 10; // utilizando nomenclatura de arrays
posiciones.x = 10 // Utizando la nomenclatura de objetos

// leer la propiedad y
console.log(posiciones['y']); // utilizando nomenclatura de arrays
console.log(posiciones.y); // Utizando la nomenclatura de objetos

// Añadir una propiedad nueva al objeto

posiciones['z'] = 0;

// ver todo el objeto (numerable)
console.log(posiciones);