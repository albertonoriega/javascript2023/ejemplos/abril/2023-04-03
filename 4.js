// Cuando pulsemos al boton, se crean cuadrados de 100x100 en el div de id lienzo
// Para ello tenemos la clase cuadrados
let boton = document.querySelector('#crear');

boton.addEventListener('click', function () {

    let lienzo = document.querySelector('#lienzo');

    // creando el div utilizando texto
    // lienzo.innerHTML += `<div class="cuadrados"></div>`;

    //vamos a crear un div usando objetos (de tipo element)
    const div = document.createElement("div");
    div.className = "cuadrados"; // Le asignamos al div la clase cuadrados
    lienzo.appendChild(div); // Al div de id lienzo le introducimos el div que hemos creado
});

