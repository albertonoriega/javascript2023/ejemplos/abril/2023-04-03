let pieza = {
    // propiedades
    x: 0,
    y: 0,
    ancho: 100,
    alto: 100,
    color: '#ccc',
    // metodos
    // destino es el lugar del documento donde quieres que dibuje
    dibujar: function (destino) {
        // creamos el nuevo div
        let caja = document.createElement("div");
        // Le ponemos los estilos que tenemos en el objeto pieza
        caja.style.width = pieza.ancho + "px";
        caja.style.height = pieza.alto + "px";
        caja.style.backgroundColor = pieza.color;
        caja.style.position = 'absolute';
        caja.style.left = pieza.x + 'px';
        caja.style.top = pieza.y + 'px';
        //Mostramos el div creado en el body
        document.querySelector(destino).appendChild(caja);
    },
    // Metodo que se va a ejecutar automáticamente cuando intente tratar el objeto como un texto
    toString: function () {
        let salida = "";
        salida = `${this.ancho}, ${this.alto}, ${this.x}, ${this.y}, ${this.color}`;
        return salida;
    }
};

pieza.dibujar('body');

//Cambiamos algunas propiedades de la pieza
pieza.x = 500;
pieza.y = 100;
pieza.width = 200;

pieza.dibujar('body');

